<!DOCTYPE HTML>
<link rel="stylesheet" type="text/css" href="support/css/general.css"/>

<?php
    session_start();
    include_once "support/support.php";
    require "support/config.php";

    $messageHistory = "";
    $messageTarget;
    $messageBody;
    $sessionUser = $_SESSION['username'];
    $limit = 5;
    $_SESSION['sessionTarget'] = "";

    if (isset($_POST['messageHeader'])) {
        if($_POST['messageHeader'] != "") {
            $messageTarget = $_POST['messageHeader'];
            $messageBody = $_POST['messageBody'];
            $query = "INSERT INTO Message(userSource, userTarget, body)
                VALUES((SELECT ID FROM User WHERE username LIKE '$sessionUser'), " . "(SELECT ID FROM User WHERE username LIKE '$messageTarget'), " .   "'$messageBody');";
        
            $result = mysqli_query($connection, $query);
            if (!$result) {
                die ("Could not query the message table in the database: <br />" . mysql_error());
            }
            $_SESSION['sessionTarget'] = $messageTarget;
        }
    }
    
    if (isset($_SESSION['lastupdated'])) {
        $lastupdated = $_SESSION['lastupdated'];
        $messageTarget = $_SESSION['sessionTarget'];
        $query = "SELECT (SELECT username FROM User WHERE ID LIKE userSource) AS userSource, body, lastupdated FROM Message
                WHERE (userTarget = (SELECT ID FROM User WHERE username LIKE '$sessionUser') OR
                userSource = (SELECT ID FROM User WHERE username LIKE '$sessionUser')) AND
                lastupdated > '$lastupdated'
                ORDER BY lastupdated DESC;";
        $result = mysqli_query($connection, $query);
        if (!$result) {
            die ("Could not query the message table in the database: <br />" . mysql_error());
        } 
    }
    else {
        $query = "SELECT (SELECT username FROM User WHERE ID LIKE userSource) AS userSource, body, lastupdated FROM Message
                WHERE userTarget = (SELECT ID FROM User WHERE username LIKE '$sessionUser')
                ORDER BY lastupdated DESC;";
        $result = mysqli_query($connection, $query);
        if (!$result) {
            die ("Could not query the message table in the database: <br />" . mysql_error());
        } 
    }
    ?>
    
    <table width="50%" cellpadding="20" cellspacing="20">
        <?php
        while ($result_row = mysqli_fetch_array($result)) {
                $userSource = $result_row['userSource'];
                $body = $result_row['body'];
                $lastupdated = $result_row['lastupdated'];
                $_SESSION['lastupdated'] = $lastupdated;
        ?>
        <tr>
            <h5>
                <?php
                    $messageHistory .= "<b>FROM: </b>" . $userSource . "<br>";
                    $messageHistory .= "<b>DATE: </b>" . $lastupdated . "<br>";
                ?>
            </h5>
            <p>
                <?php
                    $messageHistory .= $body . "<br> <br>";
                ?>
            </p>
        </tr>
        <?php
            }
            $_SESSION['messageHistory'] = $messageHistory . $_SESSION['messageHistory'];
            echo  $_SESSION['messageHistory'];
        ?>
        </table>