<?php
    session_start();
    include_once "support.php";

    /******************************************************
     *
     * upload document from user
     *
     *******************************************************/

    $username = $_SESSION['username'];
    require "config.php";


    //Create Directory if doesn't exist
    if (!file_exists(dirname(__FILE__) . '/../upload/')) {
        mkdir(dirname(__FILE__) . '/../upload/');
        chmod(dirname(__FILE__) . '/../upload/', 0755);
    }
    $dirfile = dirname(__FILE__) . '/../upload/' . $username . '/';
    if (!file_exists($dirfile))
        mkdir($dirfile);
    chmod($dirfile, 0755);
    if ($_FILES["file"]["error"] > 0) {
        $result = $_FILES["file"]["error"];
    } //error from 1-4
    else {
        $upfile = $dirfile . urlencode($_FILES["file"]["name"]);
        $title = $_POST['title'];
        $description = $_POST['description'];
        $keywords = $_POST['keywords'];
        $privacy = $_POST['privacy'];
        $category = $_POST['category'];
        $comments = $_POST['comments'];


        if (file_exists($upfile) && false)//WILL NEVER FIRE!!!!
        {
            $result = "5"; //The file has been uploaded.
        } else {
            if (is_uploaded_file($_FILES["file"]["tmp_name"])) {
                if (!move_uploaded_file($_FILES["file"]["tmp_name"], $upfile)) {
                    $result = "6"; //Failed to move file from temporary directory
                } else /*Successfully upload file*/ {
                    $userIDResult = mysqli_query($connection, "select ID from User where username = '$username'");
                    $userIDResult = mysqli_fetch_array($userIDResult);
                    $userIDResult = $userIDResult['ID'];
                    //insert into media table
                    //$uploadPath = substr($upfile, strpos($upfile, "/") + 12);
                    $uploadPath = "support/../upload/".$username."/". urlencode($_FILES["file"]["name"]);
                    $insert = "insert into Media(filename,user,type, mediaPath, title, description, keywords, viewability, category, commentable)" .
                        "values('" . urlencode($_FILES["file"]["name"]) . "',$userIDResult,'" . $_FILES["file"]["type"] . "', '$uploadPath', '" . $title . "', '" . $description . "', '" . $keywords . "', '" . $privacy . "', '" . $category . "','" . $comments . "' )";

                    echo '</br>' . $insert . '</br>';
                    $queryresult = mysqli_query($connection, $insert)
                    or die("Insert into Media error in media_upload_process.php " . mysqli_connect_error());
                    $result = "0";
                    mysqli_close($connection);
                    chmod($upfile, 0644);
                }
            } else {
                $result = "7"; //upload file failed
            }
        }
    }
    echo $result;
    //You can process the error code of the $result here.
?>

<meta http-equiv="refresh" content="0;url=../browse.php?result=<?php echo $result; ?>">
