<?php
    session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->


    <!--<link rel="stylesheet" type="text/css" href="css/margins.css"/>-->
    <link rel="stylesheet" href="css/video.css">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Media Upload</title>


    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<?php
    include_once "support.php";
    require_once "config.php";
    bootstrap();

    $categories = Array();
    //let's get the categories before we start
    if ($result = mysqli_query($connection, "SELECT ID, name FROM Category")) {
        while ($row = mysqli_fetch_array($result)) {
            $categories[$row['name']] = $row['ID'];
        }
    }
    //this should never fail.. we won't check if it did

?>
<body>
<video id="full" autoplay poster="./earthBeam.jpg" id="bgvid" loop>

    <source src="./earthBeam.mp4" type="video/mp4">
</video>
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2"
             style="background-color: rgba(255,255,255,.75); border-radius: 5px; margin-top: 10px">
            <h3 class="text-center" style="margin:0; padding:2px; margin-top: 10px;">
                <input type="hidden" name="MAX_FILE_SIZE" value="10485760"/>
                Upload A New File
                <label class="text-muted small"> (Size limit 10M)</label>
            </h3>
            <br/>

            <form method="post" action="media_upload_process.php" enctype="multipart/form-data">

                <div class="form-group">
                    <label for="title">Title:</label>
                    <input type="text" class="form-control" name="title" placeholder="Title"/>
                </div>

                <div class="form-group">
                    <label for="description">Description: </label>
                    <textarea class="form-control" name="description"></textarea>
                </div>

                <div class="form-group">
                    <label for="keywords">Keywords:</label>
                    <input type="text" name="keywords" class="form-control"/>
                </div>

                <div class="form-group">
                    <label for="privacy">Privacy Level
                    </label>
                    <select class="form-control" name="privacy">
                        <option value="public">Public</option>
                        <option value="private">Private</option>
                    </select>
                </div>
                <div class="form-group">

                    <label for="category">Category
                    </label>
                    <select class="form-control" name="category">
                        <?php
                            foreach ($categories as $key => $value) {
                                echo "<option value=" . $value . ">" . $key . "</option>";
                                //$mykey = $key;
                            }
                        ?>
                    </select>
                </div>
                <div class="form-group">

                    <label for="comments">Allow Comments?
                    </label>
                    <select class="form-control" name="comments" id="commentsSelect">
                        <option value="yes">Yes</option>
                        <option value="no">No</option>
                    </select>
                </div>

                <!--This one works for sure. the other one looks better -->
                <div class="form-group">
                    <label class="control-label" for="file">Select A File to Upload</label>
                    <input name="file" type="file" size="50" class="form-control"/>
                </div>

                <div class="form-group">
                    <input value="Upload" name="submit" type="submit" class="btn btn-primary"/>
                    <a href="../browse.php" class="btn btn-default">Cancel</a>
                </div>

            </form>
        </div>
    </div>
    <br/>
    <br/>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>

</body>
</html>
