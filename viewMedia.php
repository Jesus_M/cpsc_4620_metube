<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
    session_start();
    include_once "./support/support.php";
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>View Media</title>
    <link rel="stylesheet" href="support/css/video.css">
    <!--<link rel="stylesheet" type="text/css" href="support/css/margins.css"/>-->
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="support/css/bootstrap3_player.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="support/css/bootstrap.min.css"/>

</head>
<?php
    bootstrap();
?>
<body>
<!--simple nav-->
<div class="navbar navbar-default navbar-static-top "
     style="margin-left: 0; margin-bottom: 0; background-color: #FFFFFF">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="browse.php" class="navbar-brand"> Welcome <b><?php echo $_SESSION['username']; ?> </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>


        <div class="collapse navbar-collapse ">

            <!--NO searching here-->

            <!--search channel-->
            <!--<form id="tfnewsearch" class="navbar-form" method="post"-->
            <!--      action="--><?php //echo "myChannel.php?increment=0"; ?><!--">-->
            <!--    <div class="input-group">-->
            <!--        <input type="text" id="tfq" class="tftextinput2" name="q" size="21" maxlength="120"-->
            <!--               placeholder="Search myChannel">-->
            <!--        <input type="submit" value=">" class="tfbutton2">-->
            <!--    </div>-->
            <!--</form>-->

            <!--links -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href='browse.php'>Home</a></li>
                <li><a href='support/media_upload.php'>Upload File</a></li>
                <li><a href='MediaEdit/myChannel.php'>My Channel Items</a></li>
                <li><a href="userProfile.php">Edit Profile</a></li>
                <li><a href="index.php">Logout</a></li>

            </ul>


        </div>
    </div>
</div>


<!--style="background-color:#FFFFFF;opacity: 0.7;"-->


<video id="full" autoplay poster="./support/earthBeam.jpg" id="bgvid" loop>

    <source src="./support/earthBeam.mp4" type="video/mp4">
</video>
<!--<a href="./browse.php" class="btn btn-primary">Back to browse</a>-->
<a href="./playlists.php" class="btn btn-success">To Playlists</a>
<a href="./subscriptions.php" class="btn btn-warning">To Subscriptions</a>
<div class="container">

    <style>
        img {
            max-width: 1000px;
            max-height: 1000px;
            display: block;
        }

        .video2 {
            max-width: 1000px;
            max-height: 1000px;
            display: block;
        }

        audio {
            max-width: 1000px;
            max-height: 400px;
        }

        h4 {
            padding-bottom: 0;
            margin-bottom: 0;
            padding-top: 0;
            margin-top: 0;
        }

        td {
            padding: 1px;
        }

        div.com {
            background-color: #AA9539;
            clear: left;
            align: middle;
            opacity: 0.9;
            border-radius: 25px;
            padding: .25cm;
            width: 50%;
        }

    </style>

    <?php
        require "./support/config.php";
        if (mysqli_connect_errno($connection))
            echo mysqli_connect_error;
        $offset = 0;
        $total;
        $limit = 4;
        $apd = "./";
        if (isset($_GET['increment']))
            $offset = $limit * $_GET['increment'];
        $query = "SELECT * from Media " .
            "where deleted=0 " .
            "AND Media.ID=" . $_POST["mediaID"];
        //echo $query;
        $U = $_SESSION['username'];
        $mI = $_POST["mediaID"];

        $queryLikes = "
	select
	(select count(*) from Rating where user=(select ID from User where username=\"$U\") and deleted=0
		and mediaID=$mI and liked=1 ) as likes,
	(select Round(SUM(score)/count(*) ) from Rating where mediaID=$mI and deleted=0) as avgScore";

        $queryFav = "
	select count(*) as favorited from Favorite where user=(select ID from User where username=\"$U\" and deleted=0)
		and mediaID=$mI and deleted=0";

        $querySub = "
	select count(*) as subscribed, (select User.ID from User join Media on Media.user=User.ID where Media.ID=$mI) as targetUser from Subscription where user=(select ID from User where username=\"$U\" and deleted=0)
		and targetUser=(select User.ID from User join Media on Media.user=User.ID where Media.ID=$mI) and deleted=0";

        $queryPlay = "
	select name, ifnull((select name from Playlist where mediaID=$mI and user=(select ID from User where username=\"$U\" and deleted=0)),'none') as current
		from Playlist where user=(select ID from User where username=\"$U\" and Playlist.deleted=0) group by name
	";


	$queryComment="
	select Comment.ID,(select username from User where ID=Comment.user) as username, Comment.comment,Comment.lastUpdated from Comment

	left join Media on Media.ID=Comment.mediaID

    where mediaID=$mI and Media.commentable=1 and Media.deleted=0 and Comment.deleted=0
	order by lastUpdated DESC

	";

        //echo $queryPlay;

        //echo $queryLikes;
        $result = mysqli_query($connection, $query);
        $likeResult = mysqli_query($connection, $queryLikes);
        $favResult = mysqli_query($connection, $queryFav);
        $subResult = mysqli_query($connection, $querySub);
        $playResult = mysqli_query($connection, $queryPlay);
        $commentResult = mysqli_query($connection, $queryComment);
        if (!($result && $likeResult && $favResult && $commentResult)) {
            die ("Could not query the media table in the database: <br />" . mysqli_error($connection));
        } else
            mysqli_close($connection);

        $result_row = mysqli_fetch_array($result);
        $like_row = mysqli_fetch_array($likeResult);
        $fav_row = mysqli_fetch_array($favResult);
        $sub_row = mysqli_fetch_array($subResult);
        $title = $result_row['title'];
        $filename = $result_row['filename'];
        $filenpath = $result_row['mediaPath'];
        $type = $result_row['type'];
        $view = $result_row['viewability'];
        $keys = $result_row['keywords'];
        $descr = $result_row['description'];
        $downloads = $result_row['downloads'];
        $commentable = $result_row['commentable'];
        $likes = $like_row['likes'];
        $views = $result_row['views'];
        $avgScore = $like_row['avgScore'];
        $commentable = $result_row['commentable'];
        $commentable = $commentable + 0;
        $filenpath = str_replace("%25", "%2525", $filenpath);

        $favorited = $fav_row['favorited'];
        $subscribed = $sub_row['subscribed'];
        $MediaSource = $sub_row['targetUser'];

        $favMessage = "Favorite";
        $favType = "primary";
        if ($favorited) {
            $favMessage = "Un-" . $favMessage;
            $favType = "warning";
        }

        $subMessage = "Subscribe";
        $subType = "primary";
        if ($subscribed) {
            $subMessage = "Un-" . $subMessage;
            $subType = "warning";
        }
    ?>

    <p align="middle">
        <?php
            if (strpos($type, "image") !== false) {
                echo("<img src=\"" . $apd . $filenpath . "\" alt=\"" . $title . "\">");
            }
            if (strpos($type, "audio") !== false) {
                echo("
	<div class=\"row col-md-60 col-md-offset-30 col-xs-20\">
		<audio controls>
	  <source src=\"" . $apd . $filenpath . "\" type=\"" . $type . "\">
	Your browser does not support the audio element.
	</audio>
	</div>
	");
            }
            if (strpos($type, "video") !== false) {
                echo("<video controls>
	  <source src=\"" . $apd . $filenpath . "\" type=\"" . $type . "\" class=\"video2\">
	Your browser does not support HTML5 video.
	</video>");

            }
        ?>
    </p>
    </br></br>
    <table class="table">
        <td>
            <button onclick="download('<?php echo $filenpath; ?>')" class="btn btn-primary"
                    name="<?php echo $_POST["mediaID"]; ?>" download>Download
            </button>
        </td>
        <td>
            <button onclick="like(this)" class="btn btn-primary" name="<?php echo $_POST["mediaID"]; ?>">Like</button>
        </td>
        <td>
            <div class="dropdown">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Rate
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <li><a onclick="rate(1)">1</a></li>
                    <li><a onclick="rate(2)">2</a></li>
                    <li><a onclick="rate(3)">3</a></li>
                    <li><a onclick="rate(4)">4</a></li>
                    <li><a onclick="rate(5)">5</a></li>
                </ul>
            </div>
        </td>
        <td>
            <button onclick="favorite(this)" class="btn btn-<?php echo $favType ?>"
                    name="<?php echo $_POST["mediaID"]; ?>"><?php echo $favMessage ?></button>
        </td>
        <td>
            <button onclick="subscribe(this)" class="btn btn-<?php echo $subType ?>"
                    name="<?php echo $_POST["mediaID"]; ?>"><?php echo $subMessage ?></button>
        </td>


        <!--Playlist!!!==========================================================================-->

        <td>

            <input type="text" id="newPlaylist">
            <div class="input-append btn-group">
                <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"
                        id="playListButton">
                    Playlist
                    <span class="caret"></span></button>
                <ul class="dropdown-menu">
                    <?php

                        $noneset = 0;
                        while ($playRow = mysqli_fetch_array($playResult)) {
                            $name = $playRow['name'];

                            if (strcmp('none', $playRow['current']) == 0 && $noneset == 0) {
                                echo "<li style=\"background-color: #ffff00;\"><a onclick=\"Dplaylist('none')\">None</a></li>";
                                $noneset = 1;
                            }
                            if (strcmp($name, $playRow['current']) == 0) {
                                echo "<li style=\"background-color: #ffff00;\"><a onclick=\"playlist('$name')\">$name</a></li>";
                            } else {
                                echo "<li ><a onclick=\"playlist('$name')\">$name</a></li>";
                            }
                        }
                        if (!$noneset)
                            echo "<li style=\"background-color: #FF4B00;\"><a onclick=\"Dplaylist('none')\">Select none</a></li>";

                    ?>
                    <li>
                        <button class="btn-primary" onclick="newPlaylist('newPlaylist')" type="button">Create New
                        </button>
                    </li>


                    <!--li><a onclick="rate(1)">1</a></li>
                    <li><a onclick="rate(2)">2</a></li>
                    <li><a onclick="rate(3)">3</a></li>
                    <li><a onclick="rate(4)">4</a></li>
                    <li><a onclick="rate(5)">5</a></li-->
                </ul>
            </div>
        </td>
    </table>


    <table class="table">
        <td style="background-color:#AA9539;opacity: 0.7;">
            <h3 id="downloadLabel"> Downloads: <?php echo $downloads; ?></h3>
        </td>
        <td style="background-color:#AA9539;opacity: 0.7;">
            <h3> Likes: <?php echo $likes; ?></h3>
        </td>
        <td style="background-color:#AA9539;opacity: 0.7;">
            <h3> Views: <?php echo $views + 1; ?></h3>
        </td>
        <td style="background-color:#AA9539;opacity: 0.7;">
            <h3> Average Score: <?php echo $avgScore; ?></h3>
        </td>
        <!--td style="background-color:#AA9539;opacity: 0.7;">
		<h3> Favorited by: <?php // echo $favorites;?> people</h3>
	</td-->
    </table>


    <h2 style="background-color:#AA9539;opacity: 0.7;"> <?php echo $title; ?> </h2>
    </br></br>

    </br>
    <h3 style="background-color:#AA9539;opacity: 0.7;font-family:Georgia;"><?php echo $descr; ?></h3>
    <hr style="width: 100%; color: black; height: 1px; background-color:#522D80;"/>


    <?php if ($commentable) { ?>
        <div>
            <h2>Comment Body</h2> <h4><i>2000 character limit</i></h4>
        <textarea type="text" class="form-control input-lg" name="CommentText" id="CommentText"
                  maxlength="2000"></textarea>

            <button onclick="addComment()" value="Add Comment" class="btn btn-primary">Add Comment</button>
            </br></br>
        </div><?php } ?>
    <table width="75%" cellpadding="20" cellspacing="20">
        <?php
            while ($com_row = mysqli_fetch_array($commentResult)) {
                $ID = $com_row['ID'];
                $username = $com_row['username'];
                $comment = $com_row['comment'];
                $lastUpdated = $com_row['lastUpdated'];

                ?>
                <tr valign="top" id="<?php echo $lastUpdated . $username; ?>">
                    <div class="com">
                        <h3>
                            <?php
                                echo "$username @$lastUpdated";
                            ?>
                            <button class="btn btn-danger"
                                    onclick="deleteComment('<?php echo $ID; ?>','<?php echo $lastUpdated . $username; ?>' )">
                                Delete
                            </button>
                        </h3>
                        </br>
                        <h4>
                            <?php
                                echo $comment;
                            ?>
                        </h4>
                    </div>
                    </br>
                </tr>
                <?php
            }
        ?>
    </table>

</div>
</body>


<script>
    /*function rate(rating) {
     console.log(rating);
     //document.getElementById(me.name + "Form").submit();
     }*/


    incrementViews();
    function incrementViews() {
        var mediaID = <?php echo $_POST["mediaID"];?>;
        //console.log(playlist);
        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/incrementViews.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
        }

        console.log("Incrementing views ");
        dreq.send("mediaID=" + mediaID);
    }
    function addComment() {
        var mediaID = <?php echo $_POST["mediaID"];?>;
        var user = "<?php echo $_SESSION['username'] ?>";
        var pass = "<?php echo $_SESSION['password'] ?>";
        var comment = document.getElementById("CommentText").value;
        comment = comment.replace(/'/g, "\\'");
        //console.log(playlist);
        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/addComment.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
            location.reload();
        }

        console.log("Making Comment " + comment);
        dreq.send("mediaID=" + mediaID + "&user=" + user + "&pass=" + pass + "&comment=" + comment);
    }

    function deleteComment(ID, killMe) {
        //console.log(playlist);
        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/deleteComment.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
            location.reload();
        }
        if (confirm("Delete this comment?")) {
            console.log("Deleting Comment " + ID);
            dreq.send("ID=" + ID);

            document.getElementById(killMe).style.visibility = "hidden";
        }
    }


    function newPlaylist(me) {
        var mediaID = <?php echo $_POST["mediaID"];?>;
        var user = "<?php echo $_SESSION['username'] ?>";
        var pass = "<?php echo $_SESSION['password'] ?>";
        var playlist = document.getElementById(me).value;
        //console.log(playlist);
        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/updatePlaylist.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
        }

        console.log("Making Playlist " + playlist + " media with id " + mediaID);
        dreq.send("mediaID=" + mediaID + "&user=" + user + "&pass=" + pass + "&playlist=" + playlist);
    }

    function Dplaylist(me) {

        var mediaID = <?php echo $_POST["mediaID"];?>;
        var user = "<?php echo $_SESSION['username'] ?>";
        var pass = "<?php echo $_SESSION['password'] ?>";

        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/deletePlaylistLink.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
        }

        console.log("deleting Playlist for media with id " + mediaID);
        dreq.send("mediaID=" + mediaID + "&user=" + user + "&pass=" + pass);
    }

    function playlist(me) {
        console.log(me);

        var mediaID = <?php echo $_POST["mediaID"];?>;
        var user = "<?php echo $_SESSION['username'] ?>";
        var pass = "<?php echo $_SESSION['password'] ?>";

        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/updatePlaylist.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
        }

        console.log("Adding/updating Playlist for media with id " + mediaID);
        dreq.send("mediaID=" + mediaID + "&user=" + user + "&pass=" + pass + "&playlist=" + me);
    }

    function favorite(me) {
        var mediaID = <?php echo $_POST["mediaID"];?>;
        var user = "<?php echo $_SESSION['username'] ?>";
        var pass = "<?php echo $_SESSION['password'] ?>";

        util = "server=" + "<?php echo $server ?>" +
            "&username=" + "<?php echo $dbuser ?>" +
            "&databaseName=" + "<?php echo $database ?>" +
            "&password=" + "<?php echo $dbpass ?>";
        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/fav.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
            location.reload();
        }

        console.log("Toggling favorite with Media id " + mediaID);
        dreq.send("mediaID=" + mediaID + "&user=" + user + "&pass=" + pass + "&" + util);
    }


    function subscribe(me) {
        var user = "<?php echo $_SESSION['username'] ?>";
        var tar = "<?php echo $MediaSource;?>";
        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/subscribe.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
            location.reload();
        }

        console.log("Toggling subscription with target user " + tar);
        dreq.send("user=" + user + "&target=" + tar);
    }


    function download(me) {
        //window.open(me,'Download');

        var mediaID = <?php echo $_POST["mediaID"];?>;

        var user = "<?php echo $_SESSION['username'] ?>";
        var pass = "<?php echo $_SESSION['password'] ?>";

        util = "server=" + "<?php echo $server ?>" +
            "&username=" + "<?php echo $dbuser ?>" +
            "&databaseName=" + "<?php echo $database ?>" +
            "&password=" + "<?php echo $dbpass ?>";
        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/downloadIncr.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
            var link = document.createElement("a");
            link.download = "<?php echo $filename ?>";
            link.href = me;
            link.click();
        }

        console.log("Incrementing downloads with Media id " + mediaID);
        dreq.send("mediaID=" + mediaID + "&user=" + user + "&pass=" + pass + "&" + util);
        document.getElementById("downloadLabel").innerHTML = "Downloads: " + (<?php echo $downloads;?>+1);

    }//download

    function like(me) {
        var mediaID = <?php echo $_POST["mediaID"];?>;
        var user = "<?php echo $_SESSION['username'] ?>";
        var pass = "<?php echo $_SESSION['password'] ?>";

        util = "server=" + "<?php echo $server ?>" +
            "&username=" + "<?php echo $dbuser ?>" +
            "&databaseName=" + "<?php echo $database ?>" +
            "&password=" + "<?php echo $dbpass ?>";
        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/like.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
            location.reload();
        }

        console.log("Toggling like with Media id " + mediaID);
        dreq.send("mediaID=" + mediaID + "&user=" + user + "&pass=" + pass + "&" + util);

    }//download

    function rate(score) {
        var mediaID = <?php echo $_POST["mediaID"];?>;
        var user = "<?php echo $_SESSION['username'] ?>";
        var pass = "<?php echo $_SESSION['password'] ?>";

        util = "server=" + "<?php echo $server ?>" +
            "&username=" + "<?php echo $dbuser ?>" +
            "&databaseName=" + "<?php echo $database ?>" +
            "&password=" + "<?php echo $dbpass ?>";
        var dreq = new XMLHttpRequest();

        dreq.open("POST", "./support/databaseScripts/rate.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
            location.reload();
        }

        console.log("Rating: " + score);
        dreq.send("mediaID=" + mediaID + "&user=" + user + "&pass=" + pass + "&rating=" + score + "&" + util);

    }//download

</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="support/js/bootstrap3_player.js"></script>


</html>
