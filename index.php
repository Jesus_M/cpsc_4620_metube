<html>
<!--Earth beam video usage.
http://movietools.info/video-background-loops/misc-loops/item/loop-255-planet-beam.html

Video Repeating 
https://codepen.io/dudleystorey/pen/knqyK
-->
<head>
    <title>MeTube Login</title>
    <link rel="stylesheet" href="support/css/video.css">
    <!-- Bootstrap -->
    <link href="support/css/bootstrap.min.css" rel="stylesheet">

</head>
<?php
include_once "support/support.php";
bootstrap();
?>
<!--Body -->
<body>
<video id="full" autoplay poster="./support/earthBeam.jpg" id="bgvid" loop>
    <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->

    <source src="./support/earthBeam.mp4" type="video/mp4">
</video>
<style>
    .form-control-inline {
        min-width: 100px;
        width: auto;
    }
</style>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 " style="background-color: rgba(255,255,255,.75); border-radius: 5px; margin-top: 50px">
            <h1 class=" text-center"> Behold: MeTube! </h1>

            <?php
			session_start();
			session_unset();
			session_destroy();
			session_start();

            include_once "support/support.php";

            if (isset($_POST['submit'])) {
                if ($_POST['username'] == "" || $_POST['password'] == "") {
                    $login_error = "<p >One or more fields are missing.</p>";
                } else {
                    $check = passwordCheck($_POST['username'], $_POST['password']); // Call functions from function.php
                    if ($check == 0) {
                        $login_error = "Incorrect username or password!";
                    } else if ($check == 1) {
                        $_SESSION['username'] = $_POST['username']; //Set the $_SESSION['username']
						$_SESSION['password'] = $_POST['password'];
                        header('Location: browse.php');
                    }
                }
            }
            ?>

            <form method="post" action="<?php echo "index.php"; ?>">
                <!--h2 class="text-success">Login</h2-->

                <!-- Username-->
                <div class="form-group">
                    <label class="" for="username">Username:</label>
                    <input type="text" class="form-control " name="username">
                </div>

                <!--password-->
                <div class="form-group">
                    <label class=" " for="password">Password: </label>

                    <input class="form-control " type="password" name="password">
                </div>

                <!-- submit and clear -->
                <div class="form-group ">
                    <input name="submit" type="submit" value="Login" class="btn btn-primary form-control">

                    <!--                    <input name="clear" type="reset" value="Clear" class="btn btn-warning">-->


                </div>

                <!--                <table width="100%">-->
                <!--                    <tr>-->
                <!--                        <td width="20%" class="text-success">Username:</td>-->
                <!--                        <td width="80%"><input class="form-control form-control-inline" type="text" name="username"-->
                <!--                                               style="right=50px"><br/></td>-->
                <!--                    </tr>-->
                <!--                    <tr>-->
                <!--                        <td width="20%" class="text-success">Password:</td>-->
                <!--                        <td width="80%"><input class="form-control form-control-inline" type="password" name="password"><br/>-->
                <!--                        </td>-->
                <!--                    </tr>-->
                <!--                    <tr>-->
                <!---->
                <!--                        <td>-->
                <!--                            <input name="submit" type="submit" value="Login" class="btn btn-primary">-->
                <!---->
                <!--                            <input name="clear" type="reset" value="Clear" class="btn btn-warning">-->
                <!--                        </td>-->
                <!---->
                <!--                    </tr>-->
                <!--                </table>-->

            </form>
            <br/>
            <?php
            if (isset($login_error)) {
                echo "<div id='passwd_result' class='alert alert-danger'>" . $login_error . "</div>";
            }
            ?>
            <!--form action="login.php" method="post">

                <input type="submit" class="button"  VALUE = "Log in" >
            </form-->

            <form action="register.php" method="post">
                <div class="form-group">

                    <center><label for="register" class="text-center text-muted">Don't Have an Account?</label></center>
                    <input type="submit" name="register" value="Register Here" class="btn btn-link form-control">

                </div>
            </form>
        </div>
    </div>


    <!--Bootstrap and jQuery Scripts-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="support/js/bootstrap.min.js"></script>


</body>


<div style="position: absolute; bottom: 5px; background-color: green height: 100%">
    <h3>Planet Beam Video by
        <a href="http://movietools.info/video-background-loops/misc-loops/item/loop-255-planet-beam.html">MovieTools.info</a>
    </h3>
</div>


</html>
