<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
    session_start();
    include_once "support/support.php";
    require "support/config.php";

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Media browse</title>
    <!--<link rel="stylesheet" type="text/css" href="support/css/margins.css"/>-->

    <link rel="stylesheet" type="text/css" href="support/css/general.css"/>

    <!--<link rel="stylesheet" type="text/css" href="support/css/margins.css"/>-->
    <link rel="stylesheet" type="text/css" href="support/css/test.css"/>
    <link rel="stylesheet" type="text/css" href="support/css/bootstrap.min.css"/>

    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript">
        function saveDownload(id) {
            $.post("media_download_process.php",
                {
                    id: id,
                },
                function (message) {
                }
            );
        }
    </script>
</head>

<style>

    img {
        max-width: 200px;
        max-height: 200px;
        width: 200px;
        height: 130px;
        display: block;
    }

    video {
        max-width: 200px;
        max-height: 200px;
        width: 200px;
        height: 130px;
        display: block;
    }

    audio {
        max-width: 200px;
        max-height: 200px;
    }

    h4 {
        padding-bottom: 0;
        margin-bottom: 0;
        padding-top: 0;
        margin-top: 0;
    }

    td {
        padding: 1px;
    }

</style>

<body>
<div>
    <!--NavBar-->
    <div class="navbar navbar-default navbar-static-top "
         style="margin-left: 0; margin-bottom: 0; background-color: #FFFFFF">
        <div class="container-fluid">
            <div class="navbar-header">
                <a href="" class="navbar-brand"> Welcome <b><?php echo $_SESSION['username']; ?> </a>
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>

                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>


            <div class="collapse navbar-collapse ">


                <!--search form-->
                <!--<form class="navbar-form navbar-left" id="tfnewsearch" method="post"-->
                <!--      action="--><?php //echo "browse.php?increment=0"; ?><!--">-->
                <!--    <div class="input-group ">-->
                <!--        <input type="text" id="tfq" class="tftextinput2 form-control " name="q" size="21" maxlength="120"-->
                <!--               placeholder="Search MeTube">-->
                <!--        <span class="input-group-addon">-->
                <!--            <input type="submit"  value=">" />-->
                <!--        </span>-->
                <!---->
                <!--    </div>-->
                <!---->
                <!--</form>-->
                <!--current form-->
                <!--<div id="tfheader">-->
                <form id="tfnewsearch" class="navbar-form " method="post"
                      action="<?php echo "browse.php?increment=0"; ?>">
                    <div class="input-group">
                        <input type="text" id="tfq" class="tftextinput2" name="q" size="21" maxlength="120"
                               placeholder="Search MeTube" value="<?php if (isset($sercher)) echo $searcher; ?>">
                        <input type="submit" value=">" class="tfbutton2">
                    </div>

                </form>

                <!--<div class="tfclear"></div>-->

                <!--</div>-->

                <!--links -->
                <ul class="nav navbar-nav navbar-right">
                    <li><a href='browse.php'>Home</a></li>
                    <li><a href='support/media_upload.php'>Upload File</a></li>
                    <li><a href='MediaEdit/myChannel.php'>My Channel Items</a></li>
                    <li><a href="userProfile.php">Edit Profile</a></li>
                    <li><a href="index.php">Logout</a></li>

                </ul>


            </div>
        </div>
    </div>

    <!--Main Container for the rest of the page-->
    <div class="container-fluid" id="page" style="background-color: #f1f1f1; margin-bottom: 0">
        <div class="row row-offcanvas row-offcanvas-left" style="margin-top: 0;">
            <!--<h2>Welcome <b>--><?php //echo $_SESSION['username']; ?><!--</b></h2>-->
            <!--<a href='support/media_upload.php' class="btn btn-primary">Upload File</a>-->
            <!--<a href='MediaEdit/myChannel.php' class="btn btn-info">My Channel Items</a>-->
            <!--<a href="userProfile.php" class="btn btn-primary">Edit Profile</a>-->
            <!--    todo: need to find a better way to log out. this just redirects you to the login screen-->
            <!--<a href="index.php" class="btn btn-primary">Logout</a>-->


            <!-- Search Bar -->
            <!-- Searches not by Keyword but by substring, will soon normalize to avoid case sensitive edge cases -->
            <!--<div id="tfheader">-->
            <!--    <form id="tfnewsearch" method="post" action="--><?php //echo "browse.php?increment=0"; ?><!--">-->
            <!--        <input type="text" id="tfq" class="tftextinput2" name="q" size="21" maxlength="120"-->
            <!--               placeholder="Search MeTube" value="--><?php //echo $searcher; ?><!--">-->
            <!--        <input type="submit" value=">" class="tfbutton2">-->
            <!--    </form>-->
            <!--    --><?php //if (isset($_POST['category']) || isset($_SESSION['category'])) {
                //        echo "<h4>Category:$tmp</h4></br>";
                //    } ?>
            <!--    <div class="tfclear"></div>-->
            <!---->
            <!--</div>-->


            <!--"SIDEBAR"-->
            <div class="col-xs-6 col-sm-2 sidebar-offcanvas" style="border: 1px solid lightgray;" id="sidebar">
                <p class="text-center lead">Categories</p>
                <hr/>
                <!--Categories=====================================================================-->
                <ul class="nav">
                    <li>
                        <form method="post">
                            <span class="input-group glyphicon glyphicon"></span>
                            <input name="category" type="submit" value="All" class="btn btn-link form-control text-left"
                                   style="margin-left=1;">
                        </form>
                    </li>
                    <li>
                        <form method="post">
                            <input name="category" type="submit" value="Gaming" class="btn btn-link form-control"
                                   style="margin-left=1;">
                        </form>

                    </li>
                    <li>
                        <form method="post">
                            <input name="category" type="submit" value="Music" class="btn btn-link form-control"
                                   style="margin-left=1;">
                        </form>
                    </li>
                    <li>
                        <form method="post">
                            <input name="category" type="submit" value="Space" class="btn btn-link form-control"
                                   style="margin-left=1;">
                        </form>
                    </li>
                    <li>
                        <form method="post">
                            <input name="category" type="submit" value="Nature" class="btn btn-link form-control"
                                   style="margin-left=1;">
                        </form>
                    </li>
                    <li>
                        <form method="post">
                            <input name="category" type="submit" value="Weird" class="btn btn-link form-control"
                                   style="margin-left=1;">
                        </form>
                    </li>
                    <li>
                        <form method="post">
                            <input name="category" type="submit" value="Other" class="btn btn-link form-control"
                                   style="margin-left=1;">
                        </form>
                    </li>
                    <li></li>
                </ul>

                <p class="lead text-center">Personalized Content</p>
                <hr/>

                <ul class="nav">
                    <li>
                        <form method="post" action="./playlists.php">
                            <input name="submit" type="submit" value="Playlists" class="btn btn-link form-control">
                        </form>
                    </li>
                    <li>
                        <a href="./subscriptions.php" class="btn btn-link">My Subscriptions</a>
                    </li>

                </ul>
            </div>

            <!--MAIN CONTENT-->
            <div class="col-xs-12 col-sm-8">
                <div class="container" style="background-color: white">
                    <div id='upload_result'>
                        <?php
                            if (isset($_REQUEST['result']) && $_REQUEST['result'] != 0) {
                                echo upload_error($_REQUEST['result']);
                            }
                        ?>
                    </div>


                    <?php
                        $offset = 0;
                        $total = 0;
                        $limit = 4;
                        $query = '';
                        $searchString = "";
                        $searcher = "";

                        if (isset($_SESSION['q'])) {
                            $searcher = $_SESSION['q'];
                        }
                        if (isset($_POST['q'])) {
                            $_SESSION['q'] = $_POST['q'];
                            $searcher = $_POST['q'];
                        }
                        if (isset($_GET['increment']))
                            $offset = $limit * $_GET['increment'];
                        else {
                            $_GET['increment'] = 0;
                        }

                        $cat = "";
                        $tmp = "All";
                        if (isset($_POST['category']) || isset($_SESSION['category'])) {
                            if (isset($_POST['category'])) {
                                $_SESSION['category'] = $_POST['category'];
                                $_GET['increment'] = 0;
                                $offset = 0;
                            }
                            if (strcmp($_SESSION['category'], "All") != 0) {
                                $tmp = $_SESSION['category'];
                                $cat = " AND category = (select ID from Category where name='$tmp')";
                            }
                        }

                        if (isset($_POST['q']) || isset($_SESSION['q'])) {
                            $searchString = "AND (filename LIKE'%$searcher%' OR title LIKE'%$searcher%' OR keywords LIKE'%$searcher%')";
                        }
                        $searchString = $searchString . $cat;
                        $query = "SELECT *, (select count(*) from Media where deleted=0 $searchString ) as Total from Media where Media.deleted=0 $searchString limit $limit offset $offset";

                        //echo $query;
                        $result = mysqli_query($connection, $query);
                        if (!$result) {
                            die ("Could not query the media table in the database: <br />" . mysql_error());

                        }
                    ?>
                    <!--RANDOM DIV?-->
                    <!--<div style="background:#339900;color:#FFFFFF; width:150px;padding-left=10;margin-left=10;"></div>-->
                    <?php if (isset($_POST['category']) || isset($_SESSION['category'])) {
                        echo "<h3>Category:$tmp</h3>";
                    } ?>

                    <br/>
                    <!--Main Table that displays content-->
                    <table class="table table-responsive ">
                        <?php
                            while ($result_row = mysqli_fetch_array($result)) //filename, username, type, mediaid, path
                            {
                                $mediaid = $result_row['ID'];
                                $title = $result_row['title'];
                                $filename = $result_row['filename'];
                                $filenpath = $result_row['mediaPath'];
                                $type = $result_row['type'];
                                $total = $result_row['Total'];

                                $filenpath = str_replace("%25", "%2525", $filenpath);
                                ?>
                                <tr valign="top" style="background-color: white;">
                                    <td>
                                        <?php
                                            echo $mediaid;  //mediaid
                                        ?>
                                    </td>
                                    <td height="130px">

                                        <?php
                                            if (strpos($type, "image") !== false) {
                                                echo("<img src=\"$filenpath\" alt=\"$title\" name=\"$mediaid\" onclick=\"viewMedia(this)\">");
                                            }
                                            if (strpos($type, "audio") !== false) {
                                                echo("
						<audio controls>
						  <source src=\"$filenpath\" type=\"$type\" name=\"$mediaid\" onclick=\"viewMedia(this)\">
						Your browser does not support the audio element.
						</audio>");
                                            }

                                            if (strpos($type, "video") !== false) {
                                                echo("<video  preload=\"metadata\">
						  <source src=\"$filenpath\" type=\"$type\" >
						Your browser does not support HTML5 video.
						</video>");
                                            }
                                        ?>

                                    </td>
                                    <td>
                                        <form method="post" action="./viewMedia.php"
                                              id="<?php echo $mediaid . 'Form'; ?>">
                                            <input type="hidden" name="mediaID" value="<?php echo $mediaid; ?>"/>
                                        </form>
                                        <a onclick="viewMedia(this)" class="btn btn-primary"
                                           name="<?php echo $mediaid; ?>">View</a>


                                        <h5>
                                            <?php echo $title; ?>
                                        </h5>
                                        <!-- Following new Youtube format -->
                                    </td>
                                </tr>
                                <?php
                            }
                        ?>
                    </table>
                    <!--Table with no results-->
                    <table width="50%" cellpadding="20" cellspacing="20">
                        <td>
                            <?php
                                if ($total == 0) {
                                    echo "<p> No results found. </p>";
                                }
                            ?>
                        </td>
                    </table>

                    <br/>

                    <script> //INCREMENTATION AND DECLEMATION=====================================================================  TODO:  Fix messed up increment
                    </script>
                    <a href='./browse.php?increment=
<?php if (isset($_GET['increment'])) {
                        if ($_GET['increment'] > 0)
                            echo('' . $_GET['increment'] - 1);
                    } else
                        echo('0');
                    ?>' class="btn btn-primary"<?php $increment = 0;
                        if (isset($_GET['increment']))
                            $increment = $_GET['increment'];

                        if ($increment - 1 < 0)
                            echo('disabled');


                    ?> target='_top'>Back</a>


                    <a href='./browse.php?increment=
<?php if (isset($_GET['increment'])) {
                        if ((($_GET['increment'] + 1) * $limit) < $total) echo($_GET['increment'] + 1);
                        else  echo('0');
                    }
                    ?>' class="btn btn-primary"<?php $increment = 0;

                        if (isset($_GET['increment']))
                            $increment = $_GET['increment'];
                        if ($total == 0 || (((($increment + 1) * $limit) / $total) > 1 && ((($increment + 1) * $limit) % $total) > 0))
                            echo('disabled'); ?> target='_top'>Next</a>

                    <br><br><br>
                    <script> //INCREMENTATION AND DECLEMATION=====================================================================
                    </script>
                </div>
                <!--    end of main body content-->
            </div>
            <!--    end of row-->
        </div>
    </div>


    <!--Bootstrap and jQuery Scripts-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <!--script src="support/js/bootstrap.min.js"></script-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>


    <script>
        function viewMedia(me) {

            document.getElementById(me.name + "Form").submit();
        }

        $("#page").css("min-height", $(window).height());
        $("#sidebar").css("min-height", $(window).height());
    </script>

</div>
</body>

</html>
