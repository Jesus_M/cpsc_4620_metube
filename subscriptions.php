<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
include_once "support/support.php";
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Media browse</title>
    <!--<link rel="stylesheet" type="text/css" href="support/css/margins.css"/>-->

    <link rel="stylesheet" type="text/css" href="support/css/general.css"/>
    <link rel="stylesheet" type="text/css" href="../support/css/bootstrap.min.css"/>

	<!--<link rel="stylesheet" type="text/css" href="support/css/margins.css"/>-->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">

    <script type="text/javascript">
        function saveDownload(id) {
            $.post("media_download_process.php",
                {
                    id: id,
                },
                function (message) {
                }
            );
        }
    </script>
</head>

<style>

    img {
        max-width: 200px;
        max-height: 200px;
        width: 200px;
        height: 130px;
        display: block;
    }

    video {
        max-width: 200px;
        max-height: 200px;
        width: 200px;
        height: 130px;
        display: block;
    }

    audio {
        max-width: 200px;
        max-height: 200px;
    }

    h4 {
        padding-bottom: 0;
        margin-bottom: 0;
        padding-top: 0;
        margin-top: 0;
    }

    td {
        padding: 1px;
    }

</style>

<body>
<!--simple nav-->
<div class="navbar navbar-default navbar-static-top "
     style="margin-left: 0; margin-bottom: 0; background-color: #FFFFFF">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="../browse.php" class="navbar-brand"> Welcome <b><?php echo $_SESSION['username']; ?> </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>


        <div class="collapse navbar-collapse ">

            <!--NO searching here-->

            <!--search channel-->
            <!--<form id="tfnewsearch" class="navbar-form" method="post"-->
            <!--      action="--><?php //echo "myChannel.php?increment=0"; ?><!--">-->
            <!--    <div class="input-group">-->
            <!--        <input type="text" id="tfq" class="tftextinput2" name="q" size="21" maxlength="120"-->
            <!--               placeholder="Search myChannel">-->
            <!--        <input type="submit" value=">" class="tfbutton2">-->
            <!--    </div>-->
            <!--</form>-->

            <!--links -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href='browse.php'>Home</a></li>
                <li><a href='support/media_upload.php'>Upload File</a></li>
                <li><a href='MediaEdit/myChannel.php'>My Channel Items</a></li>
                <li><a href="userProfile.php">Edit Profile</a></li>
                <li><a href="index.php">Logout</a></li>

            </ul>


        </div>
    </div>
</div>



<div class="container" style="float:left;margin-left=1;padding-right=2px;">

    <!--<a href="./browse.php" class="btn btn-primary">Back to browse</a>-->
    <!--<br/>-->
    <!--<br/>-->

    <?php
    require "support/config.php";
    $offset = 0;
    $total = 0;
    $limit = 4;
    $query = '';

	$user = $_SESSION['username'];
	$querySub="select * from Subscription join Media on Media.user = Subscription.targetUser where Media.viewability='public' and Subscription.user=(select ID from User where username='$user')
		and Subscription.deleted=0 and Media.deleted=0
	order by Media.ID DESC";
	//echo $querySub;
    $resultSub = mysqli_query($connection, $querySub);
	if (!$resultSub) {
		die ("Could not querySub the media table in the database: <br />" . mysql_error());
	}

    ?>

    <h3>Most Recent Videos From Your Subscriptions</h3>
    <!--<div style="background:#339900;color:#FFFFFF; width:150px;padding-left=10;margin-left=10;"></div>-->
    <table class="table table-responsive table-hover" cellpadding="20" cellspacing="20" >
        <?php
        while ($result_row = mysqli_fetch_array($resultSub)) //filename, username, type, mediaid, path
        {
            $mediaid = $result_row['ID'];
            $title = $result_row['title'];
            $filename = $result_row['filename'];
            $filenpath = $result_row['mediaPath'];
            $type = $result_row['type'];

            $filenpath = str_replace("%25", "%2525", $filenpath);
            ?>
            <tr valign="top">
                <td>
                    <?php
                    echo $mediaid;  //mediaid
                    ?>
                </td>
                <td height="130px">

                    <?php
                    if (strpos($type, "image") !== false) {
                        echo("<img src=\"$filenpath\" alt=\"$title\" name=\"$mediaid\" onclick=\"viewMedia(this)\">");
                    }
                    if (strpos($type, "audio") !== false) {
                        echo("
						<audio controls>
						  <source src=\"$filenpath\" type=\"$type\" name=\"$mediaid\" onclick=\"viewMedia(this)\">
						Your browser does not support the audio element.
						</audio>");
                    }

                    if (strpos($type, "video") !== false) {
                        echo("<video  preload=\"metadata\">
						  <source src=\"$filenpath\" type=\"$type\" >
						Your browser does not support HTML5 video.
						</video>");
                    }
                    ?>

                </td>
                <td style="width: 400px">
					<form method="post" action="./viewMedia.php" id="<?php echo $mediaid . 'Form'; ?>">
                        <input type="hidden" name="mediaID" value="<?php echo $mediaid; ?>"/>
                    </form>
					<a onclick="viewMedia(this)" class="btn btn-primary" name="<?php echo $mediaid;?>">View</a>


                    <h5>
                        <?php echo $title; ?>
                    </h5>
                    <!-- Following new Youtube format -->
                </td>
            </tr>
            <?php
        }?>
    </table>

    <br/>

</div>

<script>
    function viewMedia(me) {

        document.getElementById(me.name + "Form").submit();
    }

</script>


<!--Bootstrap and jQuery Scripts-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--script src="support/js/bootstrap.min.js"></script-->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>

</html>
