<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
    session_start();
    include_once "../support/support.php";
    require "../support/config.php";

?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Media browse</title>
    <!--<link rel="stylesheet" type="text/css" href="../support/css/margins.css"/>-->
    <link rel="stylesheet" type="text/css" href="../support/css/general.css"/>
    <link rel="stylesheet" type="text/css" href="../support/css/test.css"/>

    <link rel="stylesheet" type="text/css" href="../support/css/bootstrap.min.css"/>

    <script type="text/javascript">
        function saveDownload(id) {
            $.post("media_download_process.php",
                {
                    id: id,
                },
                function (message) {
                }
            );
        }
    </script>
</head>
<?php
    bootstrap();
?>
<style>
    img {
        max-width: 200px;
        max-height: 200px;
        width: 200px;
        height: 130px;
        display: block;
    }

    video {
        max-width: 200px;
        max-height: 200px;
        width: 200px;
        height: 130px;
        display: block;
    }

    audio {
        max-width: 200px;
        max-height: 200px;
    }

    h4 {
        padding-bottom: 0;
        margin-bottom: 0;
        padding-top: 0;
        margin-top: 0;
    }

    td {
        padding: 1px;
    }
</style>

<body style="background-color: #F1F1F1">
<!--NavBar-->
<div class="navbar navbar-default navbar-static-top "
     style="margin-left: 0; margin-bottom: 0; background-color: #FFFFFF">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="../browse.php" class="navbar-brand"> Welcome <b><?php echo $_SESSION['username']; ?> </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>


        <div class="collapse navbar-collapse ">

            <!--search channel-->

            <form id="tfnewsearch" class="navbar-form" method="post"
                  action="<?php echo "myChannel.php?increment=0"; ?>">
                <div class="input-group">
                    <input type="text" id="tfq" class="tftextinput2" name="q" size="21" maxlength="120"
                           placeholder="Search myChannel">
                    <input type="submit" value=">" class="tfbutton2">
                </div>
            </form>

            <!--links -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href='../browse.php'>Home</a></li>
                <li><a href='../support/media_upload.php'>Upload File</a></li>
                <li><a href='myChannel.php'>My Channel Items</a></li>
                <li><a href="../userProfile.php">Edit Profile</a></li>
                <li><a href="../index.php">Logout</a></li>

            </ul>


        </div>
    </div>
</div>


<!--Main body Content-->
<div class="container" style="background-color: #fff;">
    <?php
        if (mysqli_connect_errno($connection))
            echo mysqli_connect_error;
        $offset = 0;
        $total;
        $limit = 4;
        if (isset($_GET['increment'])) {
            $offset = $limit * $_GET['increment'];
            $_SESSION['increment'] = $_GET['increment'];
        }

        $searchString = "";
        if (isset($_SESSION['q'])) {
            $searcher = $_SESSION['q'];
        }
        if (isset($_POST['q'])) {
            $_SESSION['q'] = $_POST['q'];
            $searcher = $_POST['q'];
        }


        if (isset($_POST['q']) || isset($_SESSION['q'])) {
            $searchString = "AND (filename LIKE'%$searcher%' OR title LIKE'%$searcher%' OR keywords LIKE'%$searcher%')";
        }


        $query = "SELECT Media.*, (select count(*) from Media join User on User.ID = Media.user where User.deleted = 0 AND Media.deleted=0 AND username='" . $_SESSION['username'] .
            "' $searchString)" .
            " as Total from Media join User on User.ID = Media.user " .
            "where User.deleted=0 AND Media.deleted=0 AND username='" . $_SESSION['username'] . "' $searchString" .
            " limit $limit offset $offset";
        //echo $query;

        $result = mysqli_query($connection, $query);
        if (!$result) {
            die ("Could not query the media table in the database: <br />" . mysqli_error($connection));
        } else
            mysqli_close($connection);


    ?>
    <h3>Your uploads</h3>

    <!--Another Random Div? -->
    <!--<div style="background:#339900;color:#FFFFFF; width:150px;"></div>-->
    <table  cellpadding="20" cellspacing="20" class="table table-responsive">
        <?php
            while ($result_row = mysqli_fetch_array($result)) //filename, username, type, mediaid, path
            {
                $mediaid = $result_row['ID'];
                $title = $result_row['title'];
                $filename = $result_row['filename'];
                $filenpath = $result_row['mediaPath'];
                $type = $result_row['type'];
                $total = $result_row['Total'];
                $apd = "../";

                $filenpath = str_replace("%25", "%2525", $filenpath);
                ?>
                <tr valign="top">
                    <td>
                        <?php
                            echo $mediaid;  //mediaid
                        ?>
                    </td>
                    <td height="130px">
                        <?php
                            if (strpos($type, "image") !== false) {
                                echo("<img src=\"" . $apd . $filenpath . "\" alt=\"" . $title . "\">");
                            }
                            if (strpos($type, "audio") !== false) {
                                echo("<audio controls>
						  <source src=\"" . $apd . $filenpath . "\" type=\"" . $type . "\">
						Your browser does not support the audio element.
						</audio>");
                            }
                            if (strpos($type, "video") !== false) {
                                echo("<video controls>
						  <source src=\"" . $apd . $filenpath . "\" type=\"" . $type . "\">
						Your browser does not support HTML5 video.
						</video>");

                            }
                        ?>

                    </td>
                    <td>
                        <button onclick="deleteClick(this)" class="btn btn-primary" name="<?php echo $mediaid; ?>">
                            Obliviate!
                        </button>
                    </td>
                    <td>
                        <form action="editMedia.php" method="post">
                            <div class="form-group">
                                <input type="hidden" class="form-control " name="mediaID"
                                       value="<?php echo $mediaid; ?>">
                            </div>
                            <div class="form-group">

                                <input type="submit" name="Edit" value="Edit" class="btn btn-primary form-control">

                            </div>
                        </form>
                    </td>
                    <td style="width: 400px">
                        <a href="<?php echo $apd . $filenpath; ?>">
                            <h4>
                                <?php echo $title; ?>
                            </h4>
                        </a>

                        <h5>
                            <?php echo $filename; ?>
                        </h5>
                        <!-- Following new Youtube format -->
                    </td>
                </tr>
                <?php
            }
        ?>
    </table>


    <script> //INCREMENTATION AND DECLEMATION=====================================================================
    </script>
    <a href='./myChannel.php?increment=
<?php if (isset($_GET['increment'])) {
        if ($_GET['increment'] > 0)
            echo('' . $_GET['increment'] - 1);
    } else echo('0'); ?>' class="btn btn-primary"<?php $increment = 0;
        if (isset($_GET['increment']))
            $increment = $_GET['increment'];
        if ($increment - 1 < 0)
            echo('disabled'); ?> target='_top'>Back</a>

    <a href='./myChannel.php?increment=
<?php if (isset($_GET['increment'])) {
        echo('' . $_GET['increment'] + 1);
    } else echo('1'); ?>' class="btn btn-primary"<?php $increment = 0;
        if (isset($_GET['increment']))
            $increment = $_GET['increment'];
        if ($total == 0 || (((($increment + 1) * $limit) / $total) >= 1))
            echo('disabled'); ?> target='_top'>Next</a>


    <br><br><br>
    <script> //INCREMENTATION AND DECLEMATION=====================================================================
    </script>

</div>


<!--Bootstrap and jQuery Scripts-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--script src="support/js/bootstrap.min.js"></script-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
</body>


<script>
    function deleteClick(me) {
        var mediaID = me.name;

        var user = "<?php echo $_SESSION['username'] ?>";
        var pass = "<?php echo $_SESSION['password'] ?>";

        util = "server=" + "<?php echo $server ?>" +
            "&username=" + "<?php echo $dbuser ?>" +
            "&databaseName=" + "<?php echo $database ?>" +
            "&password=" + "<?php echo $dbpass ?>";
        var dreq = new XMLHttpRequest();

        dreq.open("POST", "DeleteMedia.php", true);
        dreq.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        dreq.onload = function () {
            console.log(this.responseText);
            //use replace to not mess up back button history
            window.location.replace(window.location.href);
        }

        var r = confirm("Are you sure you want to murder this poor media item?");
        if (r == true) {
            console.log("Deleting Media with id " + mediaID);
            dreq.send("mediaID=" + mediaID + "&user=" + user + "&pass=" + pass + "&" + util);
        } else {
            console.log("Canceling delete");
        }

    }//delete click


</script>

</html>
