<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
    session_start();
    include_once "../support/support.php";
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Media Edit</title>
    <!--<link rel="stylesheet" type="text/css" href="../support/css/margins.css"/>-->
    <link rel="stylesheet" type="text/css" href="../support/css/general.css"/>
    <link rel="stylesheet" type="text/css" href="../support/css/test.css"/>

    <link rel="stylesheet" type="text/css" href="../support/css/bootstrap.min.css"/>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link href="../support/css/bootstrap3_player.css" rel="stylesheet">
    <style type="text/css">
        img {
            max-width: 1000px;
            max-height: 1000px;
            display: block;
        }

        video {
            max-width: 1000px;
            max-height: 1000px;
            display: block;
        }

        audio {
            max-width: 1000px;
            max-height: 400px;
        }

        h4 {
            padding-bottom: 0;
            margin-bottom: 0;
            padding-top: 0;
            margin-top: 0;
        }

        td {
            padding: 1px;
        }
    </style>
</head>
<?php
    bootstrap();
?>
<body>
<!--simple nav-->
<div class="navbar navbar-default navbar-static-top "
     style="margin-left: 0; margin-bottom: 0; background-color: #FFFFFF">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="../browse.php" class="navbar-brand"> Welcome <b><?php echo $_SESSION['username']; ?> </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>


        <div class="collapse navbar-collapse ">

            <!--NO searching here-->

            <!--search channel-->
            <!--<form id="tfnewsearch" class="navbar-form" method="post"-->
            <!--      action="--><?php //echo "myChannel.php?increment=0"; ?><!--">-->
            <!--    <div class="input-group">-->
            <!--        <input type="text" id="tfq" class="tftextinput2" name="q" size="21" maxlength="120"-->
            <!--               placeholder="Search myChannel">-->
            <!--        <input type="submit" value=">" class="tfbutton2">-->
            <!--    </div>-->
            <!--</form>-->

            <!--links -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href='../browse.php'>Home</a></li>
                <li><a href='../support/media_upload.php'>Upload File</a></li>
                <li><a href='myChannel.php'>My Channel Items</a></li>
                <li><a href="../userProfile.php">Edit Profile</a></li>
                <li><a href="../index.php">Logout</a></li>

            </ul>


        </div>
    </div>
</div>


<!--main body-->
<div class="container" style="background-color: #FFFFFF">

    <?php
        require "../support/config.php";
        if (mysqli_connect_errno($connection))
            echo mysqli_connect_error;
        $offset = 0;
        $total;
        $limit = 4;
        $apd = "../";
        if (isset($_GET['increment']))
            $offset = $limit * $_GET['increment'];
        $query = "SELECT Media.* from Media join User on User.ID = Media.user " .
            "where User.deleted=0 AND Media.deleted=0 AND username='" . $_SESSION['username'] . "'" . " AND password='" . $_SESSION['password'] . "'" .
            " AND Media.ID=" . $_POST["mediaID"] . "" .
            " limit 1";
        //echo $query;

        $result = mysqli_query($connection, $query);
        if (!$result) {
            die ("Could not query the media table in the database: <br />" . mysqli_error($connection));
        } else
            mysqli_close($connection);

        $result_row = mysqli_fetch_array($result);

        $title = $result_row['title'];
        $filename = $result_row['filename'];
        $filenpath = $result_row['mediaPath'];
        $type = $result_row['type'];
        $view = $result_row['viewability'];
        $keys = $result_row['keywords'];
        $descr = $result_row['description'];
        $commentable = $result_row['commentable'];
        $filenpath = str_replace("%25", "%2525", $filenpath);
        $cat = $result_row['category'];
    ?>


    <p align="middle">
        <?php
            if (strpos($type, "image") !== false) {
                echo("<img class='img-responsive'  src=\"" . $apd . $filenpath . "\" alt=\"" . $title . "\">");
            }
            if (strpos($type, "audio") !== false) {
                echo("
	<div class=\"row col-md-60 col-md-offset-30 col-xs-20\">
		<audio controls>
	  <source src=\"" . $apd . $filenpath . "\" type=\"" . $type . "\">
	Your browser does not support the audio element.
	</audio>
	</div>
	");
            }
            if (strpos($type, "video") !== false) {
                echo("<video controls>
	  <source src=\"" . $apd . $filenpath . "\" type=\"" . $type . "\">
	Your browser does not support HTML5 video.
	</video>");

            }
        ?>
    </p>
    </br></br>


    <!--table class="table">
	<td>
		<h3> Downloads: <?php echo $downloads; ?></h3>
	</td>
	<td>
		<h3> Likes: <?php echo $likes; ?></h3>
	</td>
	<td>
		<h3> Average Score: <?php echo $avgScore; ?></h3>
	</td>
	<td>
		<h3> Favorited by: <?php echo $favorites; ?> people</h3>
	</td>
</table-->
    <form method="post" action="<?php echo "editMediaProcess.php?mediaID=" . $_POST["mediaID"]; ?>">
        <h2>Title</h2>
        <input type="text" class="input-lg" name="Title" value="<?php echo $title; ?>">
        </br>

        <h2>View Setting</h2>
        <select name="viewable" id="viewable" class="form-control">
            <option<?php if (strcmp($view, "private") == 0) echo " selected=\"selected\""; ?>>private</option>
            <option<?php if (strcmp($view, "public") == 0) echo " selected=\"selected\""; ?>>public</option>
        </select>
        <h2>Commentable</h2>
        <select name="commentable" id="commentable" class="form-control">
            <option<?php if ($commentable == 1) echo " selected=\"selected\""; ?>>Yes</option>
            <option<?php if ($commentable == 0) echo " selected=\"selected\""; ?>>No</option>
        </select>

        <h2>Category</h2>
        <select name="category" id="category" class="form-control">
            <option value="1"<?php if ($cat == 1) echo " selected=\"selected\""; ?>>Gaming</option>
            <option value="2"<?php if ($cat == 2) echo " selected=\"selected\""; ?>>Music</option>
            <option value="3"<?php if ($cat == 3) echo " selected=\"selected\""; ?>>Space</option>
            <option value="4"<?php if ($cat == 4) echo " selected=\"selected\""; ?>>Nature</option>
            <option value="5"<?php if ($cat == 5) echo " selected=\"selected\""; ?>>Weird</option>
            <option value="6"<?php if ($cat == 6) echo " selected=\"selected\""; ?>="selected">Other</option>
        </select>

        </br>
        <h2>Keywords</h2> <h4><i>Space separated, limit 100 characters</i></h4>
        <input type="text" class="form-control input-lg" name="Keywords" id="Keywords" value="<?php echo $keys; ?>"
               maxlength="100">

        </br>
        <h2>Description</h2> <h4><i>2000 character limit</i></h4>
    <textarea type="text" class="form-control input-lg" name="Description" id="Description"
              maxlength="2000"><?php echo $descr; ?> </textarea>

        <input name="submit" type="submit" value="Submit" class="btn btn-primary form-control">
    </form>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
    <script src="../support/js/bootstrap3_player.js"></script>

</div>
</body>
</html>
















