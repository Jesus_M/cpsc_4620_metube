<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?php
session_start();
include_once "support/support.php";
?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Media browse</title>
    <link rel="stylesheet" type="text/css" href="support/css/margins.css"/>

    <link rel="stylesheet" type="text/css" href="support/css/general.css"/>

	<link rel="stylesheet" type="text/css" href="support/css/margins.css"/>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/css/bootstrap.min.css" rel="stylesheet">    
	
    <script type="text/javascript">
        function saveDownload(id) {
            $.post("media_download_process.php",
                {
                    id: id,
                },
                function (message) {
                }
            );
        }
    </script>
</head>

<style>

    img {
        max-width: 200px;
        max-height: 200px;
        width: 200px;
        height: 130px;
        display: block;
    }

    video {
        max-width: 200px;
        max-height: 200px;
        width: 200px;
        height: 130px;
        display: block;
    }

    audio {
        max-width: 200px;
        max-height: 200px;
    }

    h4 {
        padding-bottom: 0;
        margin-bottom: 0;
        padding-top: 0;
        margin-top: 0;
    }

    td {
        padding: 1px;
    }

</style>

<body>
<div class="container" style="float:left;margin-left=1;padding-right=2px;">

    <a href="./browse.php" class="btn btn-primary">Back to browse</a>

    <br/>


    <br/>

    <?php
    require "support/config.php";
    $offset = 0;
    $total = 0;
    $limit = 4;
    $query = '';
    
	$user = $_SESSION['username'];
    $query = "SELECT * from Playlist where deleted=0 and user = (select ID from User where username=\"$user\") group by name";
	$playlistName="";
	if(!isset($_POST['Favorite']))
	if(isset($_POST['playlist']))
		$playlistName = $_POST['playlist'];
	
	$query2="select * from Playlist join Media on Media.ID = Playlist.mediaID where Playlist.user = (select ID from User where username=\"$user\") and name=\"$playlistName\"
		and Media.viewability='public'
	";
	
	
	
	
	$result2 = mysqli_query($connection, $query2);
	if (!$result2) {
		die ("Could not query2 the media table in the database: <br />" . mysql_error());
	}
	
    //echo $query2;

    $result = mysqli_query($connection, $query);
    if (!$result) {
        die ("Could not query1 the media table in the database: <br />" . mysql_error());
    }

	$query3="select * from Favorite join Media on Media.ID = Favorite.mediaID where Favorite.user = (select ID from User where username=\"$user\")
		and Media.viewability='public'";
	
	$result3 = mysqli_query($connection, $query3);
	if (!$result3) {
		die ("Could not query3 the media table in the database: <br />" . mysql_error());
	}

    ?>
	
	<table width="10%" cellpadding="20" cellspacing="20" >
		<tr valign="top">
			<td style="width: 400px">
				<form method="post">
					<input name="Favorite" type="submit" value="Favorites!" class="btn btn-warning form-control">
				</form>
            </td>
		</tr>		
				
			
        <?php
        while ($result_row = mysqli_fetch_array($result)) //filename, username, type, mediaid, path
        {
            $play = $result_row['name'];
            ?>
            <tr valign="top">
                <td style="width: 400px">
					<form method="post">
                        <input name="playlist" type="submit" value="<?php echo $play;?>" class="btn btn-primary form-control">
                    </form>
                </td>
            </tr>
            <?php
        }
        ?>
    </table>
	
	
    <div style="background:#339900;color:#FFFFFF; width:150px;padding-left=10;margin-left=10;"></div>
    <table width="50%" cellpadding="20" cellspacing="20" >
        <?php
		if($result2)
        while ($result_row = mysqli_fetch_array($result2)) //filename, username, type, mediaid, path
        {
            $mediaid = $result_row['ID'];
            $title = $result_row['title'];
            $filename = $result_row['filename'];
            $filenpath = $result_row['mediaPath'];
            $type = $result_row['type'];

            $filenpath = str_replace("%25", "%2525", $filenpath);
            ?>
            <tr valign="top">
                <td>
                    <?php
                    echo $mediaid;  //mediaid
                    ?>
                </td>
                <td height="130px">
                    
                    <?php
                    if (strpos($type, "image") !== false) {
                        echo("<img src=\"$filenpath\" alt=\"$title\" name=\"$mediaid\" onclick=\"viewMedia(this)\">");
                    }
                    if (strpos($type, "audio") !== false) {
                        echo("
						<audio controls>
						  <source src=\"$filenpath\" type=\"$type\" name=\"$mediaid\" onclick=\"viewMedia(this)\">
						Your browser does not support the audio element.
						</audio>");
                    }

                    if (strpos($type, "video") !== false) {
                        echo("<video  preload=\"metadata\">
						  <source src=\"$filenpath\" type=\"$type\" >
						Your browser does not support HTML5 video.
						</video>");
                    }
                    ?>

                </td>
                <td style="width: 400px">
					<form method="post" action="./viewMedia.php" id="<?php echo $mediaid . 'Form'; ?>">
                        <input type="hidden" name="mediaID" value="<?php echo $mediaid; ?>"/>
                    </form>
					<a onclick="viewMedia(this)" class="btn btn-primary" name="<?php echo $mediaid;?>">View</a>
				

                    <h5>
                        <?php echo $title; ?>
                    </h5>
                    <!-- Following new Youtube format -->
                </td>
            </tr>
            <?php
        }
        
		
		if(isset($_POST['Favorite'])){
			unset($_POST['Favorite']);
			while ($result_row = mysqli_fetch_array($result3)) //filename, username, type, mediaid, path
			{
				$mediaid = $result_row['ID'];
				$title = $result_row['title'];
				$filename = $result_row['filename'];
				$filenpath = $result_row['mediaPath'];
				$type = $result_row['type'];

				$filenpath = str_replace("%25", "%2525", $filenpath);
				?>
				<tr valign="top">
					<td>
						<?php
						echo $mediaid;  //mediaid
						?>
					</td>
					<td height="130px">
						
						<?php
						if (strpos($type, "image") !== false) {
							echo("<img src=\"$filenpath\" alt=\"$title\" name=\"$mediaid\" onclick=\"viewMedia(this)\">");
						}
						if (strpos($type, "audio") !== false) {
							echo("
							<audio controls>
							  <source src=\"$filenpath\" type=\"$type\" name=\"$mediaid\" onclick=\"viewMedia(this)\">
							Your browser does not support the audio element.
							</audio>");
						}

						if (strpos($type, "video") !== false) {
							echo("<video  preload=\"metadata\">
							  <source src=\"$filenpath\" type=\"$type\" >
							Your browser does not support HTML5 video.
							</video>");
						}
						?>

					</td>
					<td style="width: 400px">
						<form method="post" action="./viewMedia.php" id="<?php echo $mediaid . 'Form'; ?>">
							<input type="hidden" name="mediaID" value="<?php echo $mediaid; ?>"/>
						</form>
						<a onclick="viewMedia(this)" class="btn btn-primary" name="<?php echo $mediaid;?>">View</a>
					

						<h5>
							<?php echo $title; ?>
						</h5>
						<!-- Following new Youtube format -->
					</td>
				</tr>
				<?php
			}
		}
        ?>
    </table>

    <br/>

</div>

<script>
    function viewMedia(me) {

        document.getElementById(me.name + "Form").submit();
    }

</script>


<!--Bootstrap and jQuery Scripts-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--script src="support/js/bootstrap.min.js"></script-->
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script> 

</html>
