<html>
<head>
    <link rel="stylesheet" href="support/css/video.css">
    <link href="support/css/bootstrap.min.css" rel="stylesheet">

</head>
<?php
    include_once "support/support.php";
    bootstrap();
?>
<body>
<video id="full" autoplay poster="./support/earthBeam.jpg" id="bgvid" loop>
    <!-- WCAG general accessibility recommendation is that media such as background video play through only once. Loop turned on for the purposes of illustration; if removed, the end of the video will fade in the same way created by pressing the "Pause" button  -->

    <source src="./support/earthBeam.mp4" type="video/mp4">
</video>
<?php
    session_start();


    if (isset($_POST['submit'])) {
//    simple error checking to make sure all fields were entered
        if ($_POST['username'] == "" || $_POST['email'] == "" || $_POST['pass1'] == "" || $_POST['pass2'] == "") {
            $register_error = "Please Enter All Fields.";
        } else {
            if ($_POST['pass1'] != $_POST['pass2']) {
                $register_error = "Passwords don't match. Try again?";
            } else {
                $check = user_exist_check($_POST['username'], $_POST['pass1'], $_POST['email']);
                if ($check == 1) {
                    //echo "Register succeeds";
                    $_SESSION['username'] = $_POST['username'];
                    header('Location: browse.php');
                } else if ($check == 2) {
                    $register_error = "Username already exists. Please user a different username.";
                }
            }
        }

    }

?>
<div class="container">
    <div class="row">
        <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2"
             style="background-color: rgba(255,255,255,.75); border-radius: 5px; margin-top: 50px">

            <h1 class="text-center">Registration</h1>
            <br/>

            <?php
                if (isset($register_error)) {
                    echo "<div class='alert alert-danger text-center' id='passwd_result' style='margin-top: 15px;'> register_error: " . $register_error . "</div>";
                }
            ?>
            <form action="register.php" method="post">

                <div class="form-group">
                    <input type="text" class="form-control" name="username" placeholder="Username">
                </div>
                <div class="form-group">
                    <input type="email" class="form-control" name="email" placeholder="Email">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="pass1" placeholder="Password">
                </div>
                <div class="form-group">
                    <input type="password" class="form-control" name="pass2" placeholder="Confirm Password">
                </div>
                <br/>
                <div class="form-group">
                    <input name="submit" type="submit" value="Submit" class="btn btn-primary form-control">
                </div>
                <div class="form-group">
                    <a href="index.php" class="form-control btn btn-link">Cancel</a>
                </div>

            </form>


        </div>
    </div>
</div>


<!--Bootstrap and jQuery Scripts-->

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="support/js/bootstrap.min.js"></script>


</body>
</html>
