<!DOCTYPE html>
<link rel="stylesheet" type="text/css" href="support/css/general.css"/>

<?php
    session_start();
    include_once "support/support.php";
    require "support/config.php";

    //For now i will just use the username and password to select the user but i need to figure out a way to save the users id in th post variables somehow
    $query = "SELECT ID, username, password, email, description FROM User WHERE username = '" . $_SESSION['username'] . "' AND password = '" . $_SESSION['password'] . "';";

    if ($result = mysqli_query($connection, $query)) {
        while ($row = mysqli_fetch_array($result)) {
            $_SESSION['ID'] = $row["ID"];
            //we already have username and password
            $_SESSION['email'] = $row["email"];
            $_SESSION['description'] = $row["description"];
        }

    } else {
        echo "it failed";
    }
?>


<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>User Profile</title>

    <!-- Bootstrap -->
    <link href="support/css/bootstrap.min.css" rel="stylesheet">
    
    <link rel="stylesheet" type="text/css" href="../support/css/bootstrap.min.css"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
<!--simple nav-->
<div class="navbar navbar-default navbar-static-top "
     style="margin-left: 0; margin-bottom: 0; background-color: #FFFFFF">
    <div class="container-fluid">
        <div class="navbar-header">
            <a href="browse.php" class="navbar-brand"> Welcome <b><?php echo $_SESSION['username']; ?> </a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>

                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>


        <div class="collapse navbar-collapse ">

            <!--NO searching here-->

            <!--search channel-->
            <!--<form id="tfnewsearch" class="navbar-form" method="post"-->
            <!--      action="--><?php //echo "myChannel.php?increment=0"; ?><!--">-->
            <!--    <div class="input-group">-->
            <!--        <input type="text" id="tfq" class="tftextinput2" name="q" size="21" maxlength="120"-->
            <!--               placeholder="Search myChannel">-->
            <!--        <input type="submit" value=">" class="tfbutton2">-->
            <!--    </div>-->
            <!--</form>-->

            <!--links -->
            <ul class="nav navbar-nav navbar-right">
                <li><a href='browse.php'>Home</a></li>
                <li><a href='support/media_upload.php'>Upload File</a></li>
                <li><a href='MediaEdit/myChannel.php'>My Channel Items</a></li>
                <li><a href="userProfile.php">Edit Profile</a></li>
                <li><a href="index.php">Logout</a></li>

            </ul>


        </div>
    </div>
</div>


<!-- Main container  -->
<div class="container">
    <h1 class=" text-center"><?php echo $_SESSION['username']; ?>'s Profile</h1>

    <div class="row ">
        <div class="col-md-6 pull-right">
            <h2>Recent Messages</h2>
            <?php $_SESSION['messageHistory'] = ""; ?>
            <table width="50%" cellpadding="20" cellspacing="20">
                <tr>
                    <td>
                        <form id="tfmessage" method="post" target="messageFrame" action="<?php echo "messages.php"; ?>">
                            <input type="text" id="tfmHeader" class="tftextinput4" name="messageHeader" size="21"
                                   maxlength="120"
                                   placeholder="Send to" value="">
                            <br/> <br/>
                <textarea style="height: 180px; width: 380px" type="text" id="tfmBody" class="tftextinput3"
                          name="messageBody" size="80" maxlength="120"
                          placeholder="Message" value=""> </textarea>
                            <br/> <br/>
                            <input type="submit" value="Send" class="tfbutton2">
                    </td>
                    <td>
                        <div style="padding: 40px">
                            <iframe src="messages.php" name="messageFrame" id="mFrame" frameBorder="0" width="300px"
                                    height="250px"></iframe>
                            <div>
                    </td>
                    <script type="text/javascript">
                        window.setInterval(function () {
                            document.getElementById('mFrame').contentWindow.location.href = document.getElementById('mFrame').contentWindow.location.href;
                        }, 3000);
                    </script>
                </tr>
            </table>
        </div>

        <div class="col-md-6">


            <h2>General Settings</h2>
            <a href="browse.php" class="btn btn-primary">Home</a>
            <a href="javascript:history.go(0)" class="btn btn-info"><span class="glyphicon glyphicon-refresh"></span>
                Refresh page</a>


            <a href="index.php" class="btn btn-primary">Logout</a>

            <br/>
            <br/>

            <table class="table table-responsive table-hover">
                <col width="25%">
                <col width="60%">
                <col width="15%">

                <!--username-->
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-user"></span><b> Username: </b>
                    </td>
                    <td>
                        <p id="username" class="text-muted"><?php echo $_SESSION['username']; ?></p>
                    </td>
                    <td>
                        <button type="button" class="pull-right btn btn-link" data-toggle="modal"
                                data-target="#usernameModal">Edit
                        </button>
                    </td>
                </tr>

                <!--password-->
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-lock"></span><b> Password: </b>
                    </td>
                    <td>
                        <!-- we won't actually show their password. that might be a security risk... maybe-->
                        <p class="text-muted">********</p>
                    </td>
                    <td>
                        <button type="button" class="pull-right btn btn-link" data-toggle="modal"
                                data-target="#passwordModal">Edit
                        </button>
                    </td>
                </tr>

                <!--email address-->
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-envelope"></span><b> Email Address</b>
                    </td>
                    <td>
                        <p id="email" class="text-muted"><?php echo $_SESSION['email']; ?></p>

                    </td>
                    <td>
                        <button type="button" class="pull-right btn btn-link" data-toggle="modal"
                                data-target="#emailModal">Edit
                        </button>
                    </td>
                </tr>

                <!--Description-->
                <tr>
                    <td>
                        <span class="glyphicon glyphicon-info-sign"></span><b> Description</b>
                    </td>
                    <td>
                        <p id="description" class="text-muted"><?php echo $_SESSION['description']; ?></p>
                    </td>
                    <td>
                        <button type="button" class="pull-right btn btn-link" data-toggle="modal"
                                data-target="#descriptionModal">Edit
                        </button>
                    </td>
                </tr>


            </table>

            <!--<h2>Channel Statistics</h2>-->
            <!--<p>too: make some channel statistics</p>-->

        </div>

    </div>
<!-- kill me now man-->
</div>


<!--Modals-->
<!--username modal-->
<div class="modal" id="usernameModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Edit Username</h4>
            </div>
            <div class="modal-body">

                <p><b>Current Username: </b><br/><?php echo $_SESSION['username']; ?></p>
                <br/>

                <form method="post">
                    <div class="form-group">
                        <label for="username">New Username</label>
                        <input type="text" class="form-control" name="newUsername">
                    </div>

                    <div class="form-group">
                        <label for="confirmUsername">Confirm Username</label>
                        <input type="text" class="form-control" name="confirmUsername">
                    </div>
                    <br/>

                    <div class="form-group ">
                        <input type="submit" id="submitNewUsername" name="submitNewUsername" value="Save Changes"
                               class="btn btn-primary"/>
                        <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!--password modal-->
<div class="modal" id="passwordModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Edit Password</h4>
            </div>
            <div class="modal-body">

                <p><b>Current Password: </b><br/>********</p>
                <br/>

                <form method="post">
                    <div class="form-group">
                        <label for="newPass">New Password</label>
                        <input type="password" class="form-control" name="newPass">
                    </div>

                    <div class="form-group">
                        <label for="confirmPass">Confirm Password</label>
                        <input type="password" class="form-control" name="confirmPass">
                    </div>
                    <br/>

                    <div class="form-group ">
                        <input type="submit" id="submitNewPass" name="submitNewPass" value="Save Changes"
                               class="btn btn-primary"/>
                        <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!--email modal-->
<div class="modal" id="emailModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Edit Email</h4>
            </div>
            <div class="modal-body">

                <p><b>Current Email: </b><br/><?php echo $_SESSION['email']; ?></p>
                <br/>

                <form method="post">
                    <div class="form-group">
                        <label for="newEmail">New Email Address</label>
                        <input type="email" class="form-control" name="newEmail">
                    </div>

                    <div class="form-group">
                        <label for="confirmEmail">Confirm Email Address</label>
                        <input type="email" class="form-control" name="confirmEmail">
                    </div>
                    <br/>

                    <div class="form-group ">
                        <input type="submit" id="submitNewEmail" name="submitNewEmail" value="Save Changes"
                               class="btn btn-primary"/>
                        <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<!--description modal-->
<div class="modal" id="descriptionModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button class="close" data-dismiss="modal">x</button>
                <h4 class="modal-title">Edit Description</h4>
            </div>
            <div class="modal-body">

                <p><b>Current Description: </b> <br/><?php echo $_SESSION['description']; ?></p>
                <br/>

                <form method="post">
                    <div class="form-group">
                        <label for="newDescription">New Description</label>
                        <textarea class="form-control"
                                  name="newDescription"><?php echo $_SESSION['description']; ?></textarea>
                    </div>

                    <br/>

                    <div class="form-group ">
                        <input type="submit" id="submitNewDescription" name="submitNewDescription" value="Save Changes"
                               class="btn btn-primary"/>
                        <button class="btn btn-default" data-dismiss="modal">Cancel</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<?php
    if (isset($_POST['submitNewUsername'])) {
        if ($_POST['newUsername'] == "" || $_POST['confirmUsername'] == "") {
            echo '<div class="alert alert-danger">Error: please enter a new username</div>';
        } else if ($_POST['newUsername'] != $_POST['confirmUsername']) {
            echo '<div class="alert alert-danger">Error: new username and confirmation do not match</div>';
        } else {
            // should probably check if the username is already being used. that'll come later..
            $query = "UPDATE User set username='" . $_POST['newUsername'] . "' WHERE ID=" . $_SESSION['ID'];
            if ($result = mysqli_query($connection, $query)) {
                // it was updated let's update the screen
                $_SESSION['username'] = $_POST['newUsername'];
                //todo: figure out how to update the stupid display
            }
        }
    }

    if (isset($_POST['submitNewPass'])) {
        if ($_POST['newPass'] == "" || $_POST['confirmPass'] == "") {
            echo '<div class="alert alert-danger">Error: please enter a new password</div>';
        } else if ($_POST['newPass'] != $_POST['confirmPass']) {
            echo '<div class="alert alert-danger">Error: new password and confirmation do not match</div>';
        } else {
            // should probably check if the username is already being used. that'll come later..
            $query = "UPDATE User set password='" . $_POST['newPass'] . "' WHERE ID=" . $_SESSION['ID'];
            if ($result = mysqli_query($connection, $query)) {
                // it was updated let's update the screen
                $_SESSION['password'] = $_POST['newPass'];
                //todo: figure out how to update the stupid display
            }
        }
    }

    if (isset($_POST['submitNewEmail'])) {
        if ($_POST['newEmail'] == "" || $_POST['confirmEmail'] == "") {
            echo '<div class="alert alert-danger">Error: please enter a new Email address</div>';
        } else if ($_POST['newEmail'] != $_POST['confirmEmail']) {
            echo '<div class="alert alert-danger">Error: new Email address and confirmation do not match</div>';
        } else {
            // should probably check if the username is already being used. that'll come later..
            $query = "UPDATE User set email='" . $_POST['newEmail'] . "' WHERE ID=" . $_SESSION['ID'];
            if ($result = mysqli_query($connection, $query)) {
                // it was updated let's update the screen
                $_SESSION['email'] = $_POST['newEmail'];
                //todo: figure out how to update the stupid display

            }
        }
    }

    if (isset($_POST['submitNewDescription'])) {
        if ($_POST['newDescription'] == "") {
            echo '<div class="alert alert-danger">Error: please enter a new Description</div>';
        } else {
            // should probably check if the username is already being used. that'll come later..
            $query = "UPDATE User set description='" . $_POST['newDescription'] . "' WHERE ID=" . $_SESSION['ID'];
            if ($result = mysqli_query($connection, $query)) {
                // it was updated let's update the screen
                $_SESSION['description'] = $_POST['newDescription'];
                //todo: figure out how to update the stupid display
            }
        }
    }

?>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="support/js/bootstrap.min.js"></script>


</body>
</html>
